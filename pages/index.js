import Head from "next/head";
import ListItems from "../containers/ListItems";
import NewItemForm from "../containers/NewItemForm";
import ContentBlock from "../elements/ContentBlock";
import TaskDescription from "../containers/TaskDescription";





export default function Home() {
  return (
    <div  style={{backgroundColor:"#151515",}}>
      <Head><title>The Fridge </title></Head>
      <div class="d-flex justify-content-between" style={{marginRight:10}}>
        <div  class="d-flex align-items-end" >
          <h2 style={{color:"#ddd",fontSize:20,marginLeft:30,}}>Greetings Natasha! </h2>
        </div>
        
        <div class="d-flex justify-content-center">
          <h1 className="title" style={{textAlign:"center" ,color:"#eee",fontSize:60,}}>The Fridge</h1>
          
            
          
        </div>
        <div class="d-flex align-items-end" >
        
          <img src="images/avatar.png" height={50} style={{marginBottom:5,}} /><h2 style={{marginLeft:5,color:"#ddd",fontSize:20,marginRight:10,}}>Sign Out! </h2>
        </div>
      
      </div>

     
        
          <div class="d-flex justify-content-center" style={{backgroundColor:"#1c1c1c",}}>
            <div class="d-flex align-items-center justify-content-center" style={{height:620,backgroundColor:"#1c1c1c",flex:1,}}><NewItemForm /></div>
            <div class="p-2 bd-highlight " style={{backgroundColor:"#121212",flex:1}}><ListItems /></div>
          </div>
      
    </div>
  );
}

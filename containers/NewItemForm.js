import * as React from "react";
//import n from './containers/n.png';




const  addItem = async (e, formData) => {
  console.log(e);
  let data = JSON.stringify({
    name: formData.name,
    expiresAfterSeconds: formData.time
  });
  console.log(formData.time);
  if(isNaN(formData.time)){
    alert("Not a number");
    return;
  }
  let response = await fetch(
    '/api/add-item',
    {
      method:'POST', 
      headers: { 'Content-Type': 'application/json' },
      body: data
    }
  );
 if(response.status==400){
  alert("This item already inside the fridge");
 } else {
  location.reload();
 }
 
}

function isNumber(val){
  return typeof val==='number' && !isNaN(val);
}



const NewItemForm = () => {

  const [name, setName] = React.useState('');
  const [time, setTime] = React.useState('');

  

  return(
  <div  class="d-flex justify-content-center flex-column align-items-center" style={{margin:10}}>
    <div>
      <h2 className="title" style={{color:"#eee",fontSize:20,marginTop:20}}>Wish To Add New Item? </h2>
    </div>
    <div>
      <img src="images/n.png" height={150} />
    </div>

    <form  className="form d-flex "  style={{margin:20,padding:40,borderRadius:10,backgroundColor:"#171717"}}>
      <div className="form-row" >
        <div className="form-group col-10">
          <label htmlFor="formItemName" style={{color:"white",}}>Item Name</label> 
           <input

            onChange={(e)=>setName(e.target.value)}
            type="text"
            className="form-control"
            id="formItemName"
            aria-describedby="itemNameHelp"
            placeholder="Milk"
          />
       
        </div>
          <small id="itemNameHelp" className="form-text text-muted" style={{marginTop:-5,marginBottom:10,marginLeft:5}}>
            Name of the food or drink you wish to add
          </small>
        
      </div>
      <div className="form-row">
        <div className="form-group col-10">
          <label htmlFor="formExpiresAfterSeconds" style={{color:"white"}}>Expires after</label>
            <div className="input-group ">
            <input
            onChange={(e)=>setTime(e.target.value)}
            type="text"
            className="form-control"
            id="formExpiresAfterSeconds"
            aria-label="Expires in"
            aria-describedby="basic-addon2"
            placeholder="34"
          />
          <div className="input-group-append">
            <span className="input-group-text" id="basic-addon2">
              seconds
            </span>  
          </div>
        </div>
      </div>
    </div>
    
  </form>

  
  <button onClick={(e)=>addItem(e, {name, time})}  type="submit" class="btn btn-primary text "
        style={{
    color: "white",
    backgroundImage: 'linear-gradient(#1245B1,#071535)',
    height:180,
    width:180,
    borderWidth:3,
    borderColor:"#0BE6F1",
    borderRadius:90,
    fontSize:22,
    }}
      >Add Item</button>
  
  </div>
)};

export default NewItemForm;

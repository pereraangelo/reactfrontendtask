import * as React from "react";
import differenceInSeconds from "date-fns/differenceInSeconds";
import { setSeconds } from "date-fns";

async function fetchItems(setItems, setSeconds) {
  const response = await fetch(`/api/get-items`);
  const jsonResponse = await response.json(); // parses JSON response into native JavaScript objects
  const items = jsonResponse.items;
  setItems(items);
  setInterval(()=>{
    setSeconds(prepareSecondObject(items));
  }, 1000)
  
}


const prepareSecondObject = (items) =>{
  let secondList = {};
  items.forEach(item=>{
    secondList[item.id] = Math.max(
      differenceInSeconds(new Date(item.expiresIn), new Date()),
      -5
    );
  });
  return secondList;
}


const ListItems = () => {
  const [items, setItems] = React.useState([]);
  const [seconds, setSeconds] = React.useState({});
  const now = new Date();

  React.useEffect(() => {
    fetchItems(setItems, setSeconds).then(()=>{
    });
  }, []);

  return (
    
    <div style={{margin:5}}>
      <div>
        <h2 class="text-center" style={{color:"#eee",fontSize:25,}}>Items in the Fridge </h2>
      </div>
      <div style={{ maxWidth: "680px" }}>
      <ul className="list-group">
        {items.map((item) => (
          <li className="list-group-item" key={item.id}
           style={{backgroundColor:"#333",color:"#eee",borderWidth:1,borderRadius:5,borderColor:"blueviolet",margin:3}}>
            <div className="container-sm" >
              <div className="row">
                
                <div className="col-6">{item.name}</div>
                <div className="col-6 text-right">
                {
                  seconds[item.id]==-5?'Expired':<>
                    Expires in{" "}
                    <span className={`badge badge-secondary ${seconds[item.id]<=0 && seconds[item.id] >-5 ?'pulse':''}`}>
                      {isNaN(seconds[item.id])?'': Math.max(seconds[item.id], 0)}{" "}
                      s
                    </span>
                  </>
                }
                  
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
    </div>
    
  );
};

export default ListItems;
